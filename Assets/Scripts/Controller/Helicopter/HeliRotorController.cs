using System;
using UnityEngine;

//rename
public class HeliRotorController : MonoBehaviour
{
	public enum Axis
	{
		X,
		Y,
		Z,
	}
	public Axis RotateAxis;
    private float _rotarSpeed;
    public float RotarSpeed
    {
        get { return _rotarSpeed; }
        set { _rotarSpeed = Mathf.Clamp(value,0,3000); }
    }

    private float rotateDegree;
    private Vector3 OriginalRotate;

    private bool toogleStartRotor = false;
    private bool toogleStopRotor = false;

    void Start ()
	{
        OriginalRotate = transform.localEulerAngles;
        RotarSpeed = 0f;
    }

	void Update ()
	{
        rotateDegree += RotarSpeed * Time.deltaTime;
	    rotateDegree = rotateDegree%360;

		switch (RotateAxis)
		{
		    case Axis.Y:
		        transform.localRotation = Quaternion.Euler(OriginalRotate.x, rotateDegree, OriginalRotate.z);
		        break;
		    case Axis.Z:
		        transform.localRotation = Quaternion.Euler(OriginalRotate.x, OriginalRotate.y, rotateDegree);
		        break;
		    default:
		        transform.localRotation = Quaternion.Euler(rotateDegree, OriginalRotate.y, OriginalRotate.z);
		        break;
		}

        this.updateRotorState(Time.deltaTime);
    }

    public void StartRotor()
    {
        toogleStartRotor = true;
        toogleStopRotor = false;
    }

    public void StopRotor()
    {
        toogleStartRotor = false;
        toogleStopRotor = true;
    }

    public void ToogleRotor(bool toogle)
    {
        if (toogle)
        {
            StartRotor();
        }
        else
        {
            StopRotor();
        }
    }

    private void updateRotorState(float deltaTime)
    {
        if (toogleStartRotor)
        {
            if (RotarSpeed < 800)
            {
                RotarSpeed += 80 * deltaTime;
            }
            else
            {
                toogleStartRotor = false;
            }
        }
        else if (toogleStopRotor)
        {
            if (RotarSpeed > 0.1f)
            {
                RotarSpeed -= 600 * deltaTime;
            }
            else
            {
                toogleStopRotor = false;
                RotarSpeed = 0;
            }
        }
    }
}
