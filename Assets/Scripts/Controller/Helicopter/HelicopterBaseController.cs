﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class HelicopterBaseController : MonoBehaviour
{
    public Rigidbody HelicopterModel;
    public HeliRotorController MainRotorController;

    protected float TurnForce = 10f;
    protected float ForwardForce = 10f;
    protected float ForwardTiltForce = 20f;
    protected float TurnTiltForce = 30f;
    protected float EffectiveHeight = 500f;

    protected float turnTiltForcePercent = 1.5f;
    protected float turnForcePercent = 10.3f;

    protected float _engineForce;
    public float EngineForce
    {
        get { return _engineForce; }
        set
        {
            if (value * 80 > 800)
            {
                MainRotorController.RotarSpeed = value * 80;
            }
            _engineForce = value;
        }
    }

    protected Vector2 hMove = Vector2.zero;
    protected Vector2 hTilt = Vector2.zero;
    protected float hTurn = 0f;
    protected float dragValueOriginal;

    public void start()
    {
        dragValueOriginal = HelicopterModel.drag;
    }

    protected void MoveProcess()
    {
        var turn = TurnForce * Mathf.Lerp(hMove.x, hMove.x * (turnTiltForcePercent - Mathf.Abs(hMove.y)), Mathf.Max(0f, hMove.y));
        hTurn = Mathf.Lerp(hTurn, turn, Time.fixedDeltaTime * TurnForce);
        HelicopterModel.AddRelativeTorque(0f, hTurn * HelicopterModel.mass, 0f);
        HelicopterModel.AddRelativeForce(Vector3.forward * Mathf.Max(0f, hMove.y * ForwardForce * HelicopterModel.mass));
    }

    protected void LiftProcess()
    {
        var upForce = 1 - Mathf.Clamp(HelicopterModel.transform.position.y / EffectiveHeight, 0, 1);
        upForce = Mathf.Lerp(0f, EngineForce, upForce) * HelicopterModel.mass;
        HelicopterModel.AddRelativeForce(Vector3.up * upForce);
    }

    protected void TiltProcess()
    {
        hTilt.x = Mathf.Lerp(hTilt.x, hMove.x * TurnTiltForce, Time.deltaTime);
        hTilt.y = Mathf.Lerp(hTilt.y, hMove.y * ForwardTiltForce, Time.deltaTime);
        HelicopterModel.transform.localRotation = Quaternion.Euler(hTilt.y, HelicopterModel.transform.localEulerAngles.y, -hTilt.x);
    }

    //Imite l'extinction de l'hélicoptere
    protected void turnOff()
    {
        this._engineForce = 0;
        HelicopterModel.drag = 0;
        MainRotorController.StopRotor();
    }

    //Imite le démarrage de l'hélicoptère
    protected void turnOn()
    {
        HelicopterModel.drag = dragValueOriginal;
        MainRotorController.StartRotor();
    }

}
