﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Assets.Helicopter.Scripts.Enum;
using System;

public class HelicopterController : HelicopterBaseController
{
    public ControlPanel ControlPanel;
    public Image FuelBar;
    public Text EngineStateLabel;

    private bool isEngineOn;
    private float fuelConsumeValue;
    private float fuelEmptyLimit;
    private bool isOnGround;

    // Initialisation des variable
	void Start ()
	{
        base.start();

        ControlPanel.KeyPressed += OnKeyPressed;
        ControlPanel.KeyPressedUp += OnKeyPressedUp;
        isOnGround = true;
        isEngineOn = false;
        fuelConsumeValue = 0.9999f;
        fuelEmptyLimit = 0.001f;
    }

    //Mise à jour à chaque frame
    void Update () {

    }

    //Mise à jour à chaque frame (une fosi que la frame est executé)
    void FixedUpdate()
    {
        if (isEngineOn)
        {
            //L'hélicopter ne fonctionne que s'il a du fuel
            if (FuelBar.fillAmount > fuelEmptyLimit)
            {
                LiftProcess();
                MoveProcess();
                TiltProcess();

                //L'hélicopter consomme du fuel lorsqu'il est en l'air uniquement
                if (!isOnGround)
                {
                    this.consumeFuel(fuelConsumeValue + 9 / 100000);
                }
            }
            else
            {
                this.helicrash();//Crash de l'hélico s'il n'a plus de fuel
            }
        }
    }

    private void OnKeyPressedUp(PressedKeyUpCode pressedKeyUpCode)
    {
        switch (pressedKeyUpCode)
        {
            case PressedKeyUpCode.ToogleEngine:
                this.toogleEngine();
                break;
        }
    }

    //Gestion
    private void OnKeyPressed(PressedKeyCode[] obj)
    {
            float tempY = 0;
            float tempX = 0;

            // stable forward
            if (hMove.y > 0)
                tempY = -Time.fixedDeltaTime;
            else
                if (hMove.y < 0)
                tempY = Time.fixedDeltaTime;

            // stable lurn
            if (hMove.x > 0)
                tempX = -Time.fixedDeltaTime;
            else
                if (hMove.x < 0)
                tempX = Time.fixedDeltaTime;

            foreach (var pressedKeyCode in obj)
            {
                switch (pressedKeyCode)
                {
                    case PressedKeyCode.SpeedUpPressed:

                        if (!isEngineOn) break;
                        EngineForce += 0.15f;
                        this.consumeFuel(fuelConsumeValue);
                        break;
                    case PressedKeyCode.SpeedDownPressed:

                        EngineForce -= 0.2f;
                        if (EngineForce < 0) EngineForce = 0;
                        break;

                    case PressedKeyCode.ForwardPressed:

                        if (isOnGround) break;
                        tempY = Time.fixedDeltaTime;
                        break;
                    case PressedKeyCode.BackPressed:

                        if (isOnGround) break;
                        tempY = -Time.fixedDeltaTime;
                        break;
                    case PressedKeyCode.LeftPressed:
                        if (isOnGround) break;
                        tempX = -Time.fixedDeltaTime;
                        break;
                    case PressedKeyCode.RightPressed:

                        if (isOnGround) break;
                        tempX = Time.fixedDeltaTime;
                        break;
                    case PressedKeyCode.TurnRightPressed:
                        {
                            if (isOnGround) break;
                            var force = (turnForcePercent - Mathf.Abs(hMove.y)) * HelicopterModel.mass;
                            HelicopterModel.AddRelativeTorque(0f, force, 0);
                        }
                        break;
                    case PressedKeyCode.TurnLeftPressed:
                        {
                            if (isOnGround) break;
                            var force = -(turnForcePercent - Mathf.Abs(hMove.y)) * HelicopterModel.mass;
                            HelicopterModel.AddRelativeTorque(0f, force, 0);
                        }
                        break;
                }
            }

            hMove.x += tempX;
            hMove.x = Mathf.Clamp(hMove.x, -1, 1);

            hMove.y += tempY;
            hMove.y = Mathf.Clamp(hMove.y, -1, 1);
    }

    private void OnCollisionEnter(Collision collision)
    {
        isOnGround = checkIsOnGround(collision);
    }

    private void OnCollisionExit(Collision collision)
    {
        isOnGround = false;
    }
    
    private bool checkIsOnGround(Collision collision)
    {
        GameObject collideObject = collision.gameObject;
        List<String> groundTagList = GroundTagEnum.getList();
        bool onGround = false;

        foreach (String tag in groundTagList)
        {
            if (collideObject.CompareTag(tag))
            {
                onGround = true;

                if (tag.Equals("HeliportRefuel"))
                {
                    FuelBar.fillAmount = 1f;
                }
                print(collideObject.tag);

                return onGround;
            }
        }

        return onGround;
    }

    //Change le statut de la variable isOnEngineOn avec sont contraire.
    private void toogleEngine()
    {
        isEngineOn = !isEngineOn;

        if (isEngineOn)
        {
            EngineStateLabel.text = "on";
            this.turnOn();
        }
        else
        {
            EngineStateLabel.text = "off";
            this.turnOff();
        }
    }

    //Système qui permet de consommer du fuel
    private void consumeFuel(float consumeValue)
    {
        //Consomme du fuel uniquement si la limite n'est pas franchie (la limite est nécessaire sinon en dessous d'un certain seuil, la valeur fillAmount restart à 0)
        if(FuelBar.fillAmount > fuelEmptyLimit && isEngineOn)
        {
            FuelBar.fillAmount = FuelBar.fillAmount * consumeValue;
        }
    }

    //Simule le crash de l'avion
    private void helicrash()
    {
        this.turnOff();
    }
}