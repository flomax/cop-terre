﻿using System;
using System.Collections.Generic;

namespace Assets.Helicopter.Scripts.Enum
{

    public class GroundTagEnum
    {
        private static List<String> list;

        public static List<String> getList()
        {
            if(list == null)
            {
                list = new List<String>();
                list.Add("GroundTag");
                list.Add("HeliportTag");
                list.Add("Heliport");
                list.Add("HeliportRefuel");
            }

            return list;
        }
    }
}
