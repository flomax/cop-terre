# README #

A simple helicopter system example.

### Features ###

* A sample helicopter example base on [Suncube sample](https://www.assetstore.unity3d.com/en/#!/content/40107)
* Improved behaviour
* HUD display
* Turn on/off engine
* Fuel consumtion system
* Auto Refuel when landing on the helipad with a fueltank

### Credits ###

Heavily inspired by [Base helicopter Controller sample by Suncube](https://www.assetstore.unity3d.com/en/#!/content/40107)

Thanks to AEgis Technologies who make the 3D helicopter model and release it free

### Other ###

Feel free to modify all my code, for any purpose.